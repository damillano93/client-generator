# GENERADOR DE FORMULARIOS

_aplicacion para la generacion de formularios con Angular 7_
_el aplicativo usa el archivo model.json para generar todos los archivos correpondientes para ejecutar el CRUD_





### Pre-requisitos 📋

_nodejs_
_Angular CLI_
_mongoDB_


### Instalación 🔧

_Clonar repositorio_

```
git clone https://gitlab.com/damillano93/client-generator
```

_entrar carpeta_

```
cd client-generator 
```
_generar formulario_

```
sh generate.sh
```
_entrar en la carpeta generada_

```
cd example
```
_instalar modulos_

```
npm install 
```
_ejecutar proyecto_

```
ng serve 
```



## API ⚙️

_para la generacion de la api debe usar el generador de api [api-generator](https://gitlab.com/damillano93/api-generator)_



## Autor✒️



* **David MIllan** - *Trabajo Inicial* - [damillano93](https://gitlab.com/damillano93)


