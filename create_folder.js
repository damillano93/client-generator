var ncp = require('ncp').ncp;
const fs = require('fs');
//leer JSON de los datos
var obj = JSON.parse(fs.readFileSync('model.json', 'utf8')); 
ncp.limit = 16;
const source = './template/base';
const destination = './' + obj.api_name + '_cliente';
ncp(source, destination, function (err) {
 if (err) {
   return console.error(err);
 }
 //console.log('done!');
});