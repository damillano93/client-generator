// imortar bibliotecas
const fs = require('fs');
const Handlebars = require('handlebars');
//leer JSON de los datos
var obj = JSON.parse(fs.readFileSync('model.json', 'utf8'));
// creacion de la carpeta del cliente
var dir = './' + obj.api_name + '_cliente/src/app';
//leer template app.component.html
var AppComponentHtml = fs.readFileSync('./template/app.component.html.txt', 'utf8')
//leer template home.component.html
var HomeComponentHtml = fs.readFileSync('./template/home.component.html.txt', 'utf8')
//leer template app.component.html
var AppComponentTs = fs.readFileSync('./template/app.component.ts.txt', 'utf8')

//leer template app-routing.module.ts
var AppRoutingModuleTs = fs.readFileSync('./template/app-routing.module.ts.txt', 'utf8')
//leer template app.module.ts
var AppModuleTs = fs.readFileSync('./template/app.module.ts.txt', 'utf8')
//lee numero de tablas de la api
var len_obj = obj.collections.length;
var CamposHtml = '';
var LinkHome = '';
var ImportTs = '';
var CamposRoutes ='';
var ImportService ='';
var PrivateService = '';
var Declarations = '';
var Providers = '';
for (var j = 0; j < len_obj; j++) {
    //nombre de la colecion en mayuscula la primera letra
    var Unombre = jsUcfirst(obj.collections[j].name)
    //nombre de la colecion en minuscula
    var nombre = obj.collections[j].name
    
    CamposHtml += 
    '<div class="d-inline-block" ngbDropdown #myDrop'+nombre+'="ngbDropdown">'+'\n'+
    '<button class="btn btn-outline-primary btn-lg btn-block" id="dropdownManual2" ngbDropdownAnchor'+'\n'+
    '(focus)="myDrop'+nombre+'.open()">'+Unombre+'</button>'+'\n'+
    '<div ngbDropdownMenu aria-labelledby="dropdownManual">'+'\n'+
    '<a routerLink="'+nombre+'/create" class="nav-link" routerLinkActive="active" sidebarjsToggle>'+'\n'+
    'Crear '+Unombre+''+'\n'+
    '</a>'+'\n'+
    '<a routerLink="'+nombre+'" class="nav-link" routerLinkActive="active" sidebarjsToggle>'+'\n'+
    'Ver '+Unombre+''+'\n'+
    '</a>'+'\n'+
    '</div>'+'\n'+
    '</div>'+'\n';
    LinkHome +=
    '<a routerLink="'+nombre+'/create" routerLinkActive="active" class="btn btn-primary">'+'\n'+
    '<i class="far fa-plus-square fa-7x"></i>'+'\n'+
    '<h5 class="card-title">Crear '+Unombre+'</h5>'+'\n'+
    '</a>'+'\n'+
    '&nbsp;'+'\n'+
    '<a routerLink="'+nombre+'" routerLinkActive="active" class="btn btn-success">'+'\n'+
    '<i class="far fa-eye fa-7x"></i>'+'\n'+
    '<h5 class="card-title">Ver '+Unombre+'</h5>'+'\n'+
    '</a>'+'\n'+
    '<p></p>'+'\n'
    ImportTs += 
    'import { '+Unombre+'AddComponent } from \'./'+Unombre+'/add-'+nombre+'/'+nombre+'-add.component\';'+'\n'+
    'import { '+Unombre+'EditComponent } from \'./'+Unombre+'/edit-'+nombre+'/'+nombre+'-edit.component\';'+'\n'+
    'import { '+Unombre+'GetComponent } from \'./'+Unombre+'/get-'+nombre+'/'+nombre+'-get.component\';'+'\n';
     
    CamposRoutes+=
      '{'+'\n'+
      '  path: \''+nombre+'/create\','+'\n'+
      '  component: '+Unombre+'AddComponent'+'\n'+
      '},'+'\n'+
      '{'+'\n'+
      '  path: \''+nombre+'/edit/:id\','+'\n'+
      '  component: '+Unombre+'EditComponent'+'\n'+
      '},'+'\n'+
      '{'+'\n'+
      '  path: \''+nombre+'\','+'\n'+
      '  component: '+Unombre+'GetComponent'+'\n'+
      '}';
      if (j != len_obj-1) {
        CamposRoutes+=','+'\n'
        Providers += ''+Unombre+'Service,'+'\n'
      }else{
        CamposRoutes+='\n'
        Providers += ''+Unombre+'Service'+'\n'
      }
      ImportService += 
      'import { '+Unombre+'Service } from \'./services/'+nombre+'.service\';'+'\n' 
      PrivateService += 
      ', private '+Unombre+'Ser: '+Unombre+'Service';
      Declarations +=
      ','+'\n'+
      ''+Unombre+'AddComponent,'+'\n'+
      ''+Unombre+'GetComponent,'+'\n'+
      ''+Unombre+'EditComponent'+'\n';
     
}
// genera el template
const template_AppComponentHtml = Handlebars.compile(AppComponentHtml, { noEscape: true });
// agrega los campos del template 
const contents_AppComponentHtml = template_AppComponentHtml({ CamposHtml:CamposHtml , Camposnombre:jsUcfirst(obj.api_name)});
//genera el archivo 
fs.writeFile('./' + dir +'/app.component.html', contents_AppComponentHtml,   err => {
    if (err) {
        return console.error(`Error: ${err.message}.`);
    }
});
//copia estios
fs.copyFile('./template/app.component.css.txt', './' + dir + '/app.component.css', (err) => {
    if (err) throw err;
    
});
// genera el template
const template_HomeComponentHtml = Handlebars.compile(HomeComponentHtml, { noEscape: true });
// agrega los campos del template 
const contents_HomeComponentHtml = template_HomeComponentHtml({ LinkHome:LinkHome});
//genera el archivo 
fs.writeFile('./' + dir +'/home/home.component.html', contents_HomeComponentHtml,   err => {
    if (err) {
        return console.error(`Error: ${err.message}.`);
    }
});
// genera el template
const template_AppRoutingModuleTs = Handlebars.compile(AppRoutingModuleTs, { noEscape: true });
// agrega los campos del template 
const contents_AppRoutingModuleTs = template_AppRoutingModuleTs({ ImportTs:unescape(ImportTs), CamposRoutes: unescape(CamposRoutes) });
//genera el archivo 
fs.writeFile('./' + dir +'/app-routing.module.ts', contents_AppRoutingModuleTs, err => {
    if (err) {
        return console.error(`Error: ${err.message}.`);
    }
});
var titulo=obj.api_name;
// genera el template
const template_AppComponentTs = Handlebars.compile(AppComponentTs, { noEscape: true });
// agrega los campos del template 
const contents_AppComponentTs = template_AppComponentTs({ ImportService:unescape(ImportService), PrivateService: unescape(PrivateService), titulo:titulo });
//genera el archivo 
fs.writeFile('./' + dir +'/app.component.ts', contents_AppComponentTs, err => {
    if (err) {
        return console.error(`Error: ${err.message}.`);
    }
});
// genera el template
const template_AppModuleTs = Handlebars.compile(AppModuleTs, { noEscape: true });
// agrega los campos del template 
const contents_AppModuleTs = template_AppModuleTs({ ImportService:unescape(ImportService), ImportTs:unescape(ImportTs) , Providers:Providers, Declarations:Declarations});
//genera el archivo 
fs.writeFile('./' + dir +'/app.module.ts', contents_AppModuleTs, err => {
    if (err) {
        return console.error(`Error: ${err.message}.`);
    }
});
function jsUcfirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}