// imortar bibliotecas
const fs = require('fs');
const Handlebars = require('handlebars');
//leer JSON de los datos
var obj = JSON.parse(fs.readFileSync('model.json', 'utf8'));
// creacion de la carpeta del cliente
var dir = './' + obj.api_name + '_cliente/src/app';
//leer template 
var edit = fs.readFileSync('./template/crud/edit/edit.component.html.txt', 'utf8')
//leer template 
var editts = fs.readFileSync('./template/crud/edit/edit.component.ts.txt', 'utf8')
//lee numero de tablas de la api
var len_obj = obj.collections.length;

for (var j = 0; j < len_obj; j++) {


    var Unombre = jsUcfirst(obj.collections[j].name)
    var nombre = obj.collections[j].name
    // creacion de la carpeta  
var colection = './' + dir + '/'+Unombre;
if (!fs.existsSync(colection)) {
    fs.mkdirSync(colection);
}
var fn_edit = './' + dir + '/'+Unombre+'/edit-'+nombre;
if (!fs.existsSync(fn_edit)) {
    fs.mkdirSync(fn_edit);
}   
    // genera el template del edit 
    const template_edit = Handlebars.compile(edit, { noEscape: true });
    // genera el template del edit 
    const template_editts = Handlebars.compile(editts, { noEscape: true });
    // agrega los campos de la tabla
    var len_campos = obj.collections[j].fields.length
    var campos = ''
    var campos2 = ''
    var campos3 = ''
    var col = ''
    var GenerateConstructor = '';
    var ImportRelation = '';
    var GetRelation ='';
    var ImportService = '';
    var ImportModel = '';
    for (var i = 0; i < len_campos; i++) {
    var tipo = 'text'    
                if(obj.collections[j].fields[i].type == "Number")
                {
                 tipo = 'number'
                } 
                if(obj.collections[j].fields[i].type == "String")
                {
                    tipo='text'
                }      
                         
                if (obj.collections[j].fields[i].type == "Relation") {   
                    campos+= 
                '<div class="form-group">'+'\n'+
                '<label class="col-md-4">'+jsUcfirst(obj.collections[j].fields[i].name)+'</label>'+'\n'+
                '<select class="form-control" formControlName="'+obj.collections[j].fields[i].name+'" #'+obj.collections[j].fields[i].name+' >'+'\n'+
                '<option  [value]="'+obj.collections[j].fields[i].name+'._id" *ngFor="let '+obj.collections[j].fields[i].name+' of '+jsUcfirst(obj.collections[j].fields[i].name)+'" >{{'+obj.collections[j].fields[i].name+'.nombre}}</option>'+'\n'+
                '</select>'+'\n'+
                '</div>'+'\n'+
                '<div *ngIf="angForm.controls[\''+obj.collections[j].fields[i].name+'\'].invalid && (angForm.controls[\''+obj.collections[j].fields[i].name+'\'].dirty || angForm.controls[\''+obj.collections[j].fields[i].name+'\'].touched)" class="alert alert-danger">'+'\n'+
                '<div *ngIf="angForm.controls[\''+obj.collections[j].fields[i].name+'\'].errors.required">'+'\n'+
                ''+jsUcfirst(obj.collections[j].fields[i].name)+' es requerido.'+'\n'+
                '</div>'+'\n'+
                '</div>'+'\n';
                ImportRelation += 'import '+jsUcfirst(obj.collections[j].fields[i].name)+' from \'../../models/'+jsUcfirst(obj.collections[j].fields[i].name)+'\';';
                ImportService +=   'import { '+jsUcfirst(obj.collections[j].fields[i].name)+'Service } from \'../../services/'+obj.collections[j].fields[i].name+'.service\';';
                GenerateConstructor += ' private '+jsUcfirst(obj.collections[j].fields[i].name)+'Ser: '+jsUcfirst(obj.collections[j].fields[i].name)+'Service,';
                GetRelation += 
                'this.'+jsUcfirst(obj.collections[j].fields[i].name)+'Ser'+'\n'+
                '.get'+jsUcfirst(obj.collections[j].fields[i].name)+'()'+'\n'+
                '.subscribe((data: '+jsUcfirst(obj.collections[j].fields[i].name)+'[]) => {'+'\n'+
                'this.'+jsUcfirst(obj.collections[j].fields[i].name)+' = data;'+'\n'+
                '});'+'\n';
                ImportModel += ''+jsUcfirst(obj.collections[j].fields[i].name)+': '+jsUcfirst(obj.collections[j].fields[i].name)+'[];';
                }else{
                campos += 
                '<div class="form-group">'+'\n'+
                '<label class="col-md-4">'+jsUcfirst(obj.collections[j].fields[i].name)+'</label>'+'\n'+
                '<input type="'+tipo+'" class="form-control" formControlName="'+obj.collections[j].fields[i].name+'" #'+obj.collections[j].fields[i].name+' [(ngModel)] = "'+nombre+'.'+obj.collections[j].fields[i].name+'"/>'+'\n'+ 
                '</div>'+'\n'+
                '<div *ngIf="angForm.controls[\''+obj.collections[j].fields[i].name+'\'].invalid && (angForm.controls[\''+obj.collections[j].fields[i].name+'\'].dirty || angForm.controls[\''+obj.collections[j].fields[i].name+'\'].touched)" class="alert alert-danger">'+'\n'+
                '<div *ngIf="angForm.controls[\''+obj.collections[j].fields[i].name+'\'].errors.required">'+'\n'+
                ''+jsUcfirst(obj.collections[j].fields[i].name)+' es requerido.'+'\n'+
                '</div>'+'\n'+
                '</div>'+'\n'
                }
                if (i != len_campos-1) {
               col += obj.collections[j].fields[i].name + '.value, '
               campos2 += obj.collections[j].fields[i].name + ', '
               campos3 += obj.collections[j].fields[i].name +': [\'\', Validators.required ],'+'\n'
                }else{
                    col += obj.collections[j].fields[i].name + '.value'
                    campos2 += obj.collections[j].fields[i].name + ' '
                    campos3 += obj.collections[j].fields[i].name +': [\'\', Validators.required ]'+'\n'
                }
    }
    campos+= 
    '<div class="form-group">'+'\n'+
    '    <button (click)="update'+Unombre+'('+col+')"'+'\n'+
    '    [disabled]="angForm.pristine || angForm.invalid"'+'\n'+
    '    class="btn btn-primary"> '+Unombre+'</button>'+'\n'+
    '  </div>'+'\n'
    // agrega los campos del template al servicio
    const contents_edit = template_edit({ fields: unescape(campos), Unombre:Unombre });
    //genera el archivo del servicio
    fs.writeFile('./' + dir + '/'+Unombre+'/edit-'+nombre+'/' + nombre + '-edit.component.html', contents_edit, err => {
        if (err) {
            return console.error(`Error: ${err.message}.`);
        }


    });
// agrega los campos del template al servicio
const contents_editts = template_editts({ name: nombre,uname:Unombre ,campos2:campos2,campos3:campos3 , ImportRelation:unescape(ImportRelation), GenerateConstructor:GenerateConstructor, ImportService:unescape(ImportService), GetRelation:GetRelation, ImportModel:ImportModel });
//genera el archivo del servicio
fs.writeFile('./' + dir + '/'+Unombre+'/edit-'+nombre+'/' + nombre + '-edit.component.ts', contents_editts, err => {
    if (err) {
        return console.error(`Error: ${err.message}.`);
    }


});
//copia estios
fs.copyFile('./template/crud/edit/edit.component.css.txt', './' + dir + '/'+Unombre+'/edit-'+nombre+'/' + nombre + '-edit.component.css', (err) => {
    if (err) throw err;
    //copia route status   
});

}


function jsUcfirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}