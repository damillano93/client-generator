// imortar bibliotecas
const fs = require('fs');
const Handlebars = require('handlebars');
//leer JSON de los datos
var obj = JSON.parse(fs.readFileSync('model.json', 'utf8'));
// creacion de la carpeta del cliente
var dir = './' + obj.api_name + '_cliente/src/app';
//leer template 
var get = fs.readFileSync('./template/crud/get/get.component.html.txt', 'utf8')
//leer template 
var getts = fs.readFileSync('./template/crud/get/get.component.ts.txt', 'utf8')
//lee numero de tablas de la api
var len_obj = obj.collections.length;

for (var j = 0; j < len_obj; j++) {


    var Unombre = jsUcfirst(obj.collections[j].name)
    var nombre = obj.collections[j].name
    // creacion de la carpeta  
var colection = './' + dir + '/'+Unombre;
if (!fs.existsSync(colection)) {
    fs.mkdirSync(colection);
}
var fn_get = './' + dir + '/'+Unombre+'/get-'+nombre;
if (!fs.existsSync(fn_get)) {
    fs.mkdirSync(fn_get);
}   
    // genera el template del get 
    const template_get = Handlebars.compile(get, { noEscape: true });
    // genera el template del get 
    const template_getts = Handlebars.compile(getts, { noEscape: true });
    // agrega los campos de la tabla
    var len_campos = obj.collections[j].fields.length
    var campos = ''
    var campos2 = ''
    var campos3 = ' <tr *ngFor="let '+nombre+' of '+nombre+'">'+'\n'
    var col = ''
    for (var i = 0; i < len_campos; i++) {
    var tipo = 'text'    
                if(obj.collections[j].fields[i].type == "Number")
                {
                 tipo = 'number'
                } 
                if(obj.collections[j].fields[i].type == "String")
                {
                    tipo='text'
                }      
                         
                campos += ' <td>'+jsUcfirst(obj.collections[j].fields[i].name)+'</td>'+'\n'
                if (i != len_campos-1) {
               col += obj.collections[j].fields[i].name + '.value, '
              
               campos3 += '<td>{{ '+nombre+'.'+obj.collections[j].fields[i].name +'}}</td>'+'\n'
                }else{
                    col += obj.collections[j].fields[i].name + '.value'
                  
                    campos3 += '<td>{{ '+nombre+'.'+obj.collections[j].fields[i].name +'}}</td>'+'\n'
                }
    }
    campos3+= '<td><a [routerLink]="[\'edit\', '+nombre+'._id]" class="btn btn-primary"><i class="far fa-edit"></i></a>&nbsp;&nbsp;<a (click) = "delete'+Unombre+'('+nombre+'._id)" class="btn btn-danger"><i class="far fa-trash-alt"></i></a></td>'

    // agrega los campos del template al servicio
    const contents_get = template_get({ campos: unescape(campos),campos3: unescape(campos3) , Unombre:Unombre });
    //genera el archivo del servicio
    fs.writeFile('./' + dir + '/'+Unombre+'/get-'+nombre+'/' + nombre + '-get.component.html', contents_get, err => {
        if (err) {
            return console.error(`Error: ${err.message}.`);
        }


    });
// agrega los campos del template al servicio
const contents_getts = template_getts({ name: nombre,uname:Unombre  });
//genera el archivo del servicio
fs.writeFile('./' + dir + '/'+Unombre+'/get-'+nombre+'/' + nombre + '-get.component.ts', contents_getts, err => {
    if (err) {
        return console.error(`Error: ${err.message}.`);
    }


});
//copia estios
fs.copyFile('./template/crud/get/get.component.css.txt', './' + dir + '/'+Unombre+'/get-'+nombre+'/' + nombre + '-get.component.css', (err) => {
    if (err) throw err;
    //copia route status   
});

}


function jsUcfirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}