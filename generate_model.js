// imortar bibliotecas
const fs = require('fs');
const Handlebars = require('handlebars');
//leer JSON de los datos
var obj = JSON.parse(fs.readFileSync('model.json', 'utf8'));
// creacion de la carpeta del cliente
var dir = './' + obj.api_name + '_cliente/src/app';

// creacion de la carpeta models 
var models = './' + dir + '/models';
if (!fs.existsSync(models)) {
    fs.mkdirSync(models);
}

//leer template del modelo 
var model = fs.readFileSync('./template/model_template.txt', 'utf8')
//lee numero de tablas de la api
var len_obj = obj.collections.length;

for (var j = 0; j < len_obj; j++) {
    var relation = ''
    var route_relation = ''
    var Unombre = jsUcfirst(obj.collections[j].name)
    var nombre = obj.collections[j].name
    // genera el template del modelo 
    const template_model = Handlebars.compile(model, { noEscape: true });
    // agrega los campos de la tabla
    var len_campos = obj.collections[j].fields.length
    var campos = ''
    
    for (var i = 0; i < len_campos; i++) {
    var import_relation = '';
        if (obj.collections[j].fields[i].type == "Relation") {
            import_relation += '// import { '+ jsUcfirst(obj.collections[j].fields[i].name)+' } from \'./'+jsUcfirst(obj.collections[j].fields[i].name)+'\' ;'
            //console.log(import_relation)
            if (obj.collections[j].fields[i].array == true) {
                campos += '// '+obj.collections[j].fields[i].name + ':' + jsUcfirst(obj.collections[j].fields[i].name) + '[];\n'
            } else {
                campos += '// '+obj.collections[j].fields[i].name + ':' + jsUcfirst(obj.collections[j].fields[i].name) + '[];\n'
            }

        } else {
            campos += obj.collections[j].fields[i].name + ': ' + obj.collections[j].fields[i].type + ';\n'
        }

    }
    // agrega los campos del template al modelo
    const contents_model = template_model({ name: Unombre, fields: unescape(campos), import_relation:unescape(import_relation) });
    //genera el archivo del modelo
    fs.writeFile('./' + dir + '/models/' + Unombre + '.ts', contents_model, err => {
        if (err) {
            return console.error(`Error: ${err.message}.`);
        }


    });



}


function jsUcfirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}