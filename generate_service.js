// imortar bibliotecas
const fs = require('fs');
const Handlebars = require('handlebars');
//leer JSON de los datos
var obj = JSON.parse(fs.readFileSync('model.json', 'utf8'));
// creacion de la carpeta del cliente

var dir = './' + obj.api_name + '_cliente/src/app';
/*
if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
}
*/
// creacion de la carpeta services 
var services = './' + dir + '/services';
if (!fs.existsSync(services)) {
    fs.mkdirSync(services);
}
var port = obj.port;
//leer template del servicio 
var service = fs.readFileSync('./template/service_template.txt', 'utf8')
//lee numero de tablas de la api
var len_obj = obj.collections.length;

for (var j = 0; j < len_obj; j++) {
    var relation = ''
    var route_relation = ''
    var Unombre = jsUcfirst(obj.collections[j].name)
    var nombre = obj.collections[j].name
    // genera el template del service 
    const template_service = Handlebars.compile(service, { noEscape: true });
    // agrega los campos de la tabla
    var len_campos = obj.collections[j].fields.length
    var campos = ''
    var campos2 = ''
    for (var i = 0; i < len_campos; i++) {
    var import_relation = '';
   
            //import_relation += 'import { '+ jsUcfirst(obj.collections[j].fields[i].name)+' } from \'./'+obj.collections[j].fields[i].name+'\' ;'
            
            if (i != len_campos-1) {
                campos += obj.collections[j].fields[i].name + ': ' + obj.collections[j].fields[i].name + ',\n'
                campos2 += obj.collections[j].fields[i].name + ' , '

        }else{
            campos += obj.collections[j].fields[i].name + ': ' + obj.collections[j].fields[i].name + '\n'
                campos2 += obj.collections[j].fields[i].name + ' '
        }

    }
    // agrega los campos del template al servicio
    const contents_service = template_service({ Uname: Unombre, name: nombre, fieldsA: unescape(campos),fieldsB: unescape(campos2), port: port });
    //genera el archivo del servicio
    fs.writeFile('./' + dir + '/services/' + nombre + '.service.ts', contents_service, err => {
        if (err) {
            return console.error(`Error: ${err.message}.`);
        }


    });



}


function jsUcfirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}